const int pSwitch = 8;

unsigned long oldTime = 0;
int switchState = 0;
int oldSwitchState = 0;
int led = 2;

long interval = 1000;


void setup() {
  for (int x = 2; x < 8; x++) {
    pinMode(x, OUTPUT);
  }

  pinMode(pSwitch, INPUT);
}


void loop() {
  unsigned long currentTime = millis();

  if (currentTime - oldTime > interval) {
    oldTime = currentTime;
    digitalWrite(led, HIGH);
    led++;

    if (led == 7) {
      
    }
  }

  switchState = digitalRead(pSwitch);

  if (switchState != oldSwitchState) {
    for (int x=2; x<8; x++) {
      digitalWrite(x, LOW);
    }

    led = 2;
    oldTime = currentTime;
  }

  oldSwitchState = switchState;
}













